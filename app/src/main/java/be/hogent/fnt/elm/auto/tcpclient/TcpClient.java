package be.hogent.fnt.elm.auto.tcpclient;

import android.annotation.TargetApi;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by giann on 2017-10-31.
 */

public class TcpClient extends Thread {

    private static final byte BUFFER_SIZE = 16;
    private static final int TIMEOUT = 5000;
    private static final String LINE_TERMINATOR = "\n\r";

    private final TcpClientCallback callback;
    private final ScheduledExecutorService executor;
    private final Object lock = new Object();

    private Socket socket;
    private BufferedWriter writer;
    private BufferedReader reader;

    public TcpClient(final TcpClientCallback callback, final ScheduledExecutorService service) {
        this.callback = callback;
        this.executor = service;
    }

    @Override
    public void run() {
        for (;;) {
            try {
                if (this.reader != null) {
                    this.callback.onMessageReceived(reader.readLine());
                }
            } catch (final Exception e) {
                if (this.reader != null) {
                    this.callback.onError(e);
                    this.close();
                }

                // wait for reconnect.
                synchronized (this.lock) {
                    try {
                        this.lock.wait();
                    } catch (final InterruptedException ie) {
                        ie.printStackTrace(System.err);
                    }
                }
            }
        }
    }

    private void close() {
        try {
            if (this.reader != null) {
                this.reader = null;
                this.writer = null;
                this.socket.close();
            }
        } catch (final IOException ioe) {
            this.callback.onError(ioe);
        } finally {
            this.callback.onClosed();
        }
    }

    public void asyncConnect(final String host, final int port) {
        this.executor.execute(() -> {
            try {
                this.socket = new Socket();
                this.socket.connect(new InetSocketAddress(host, port), TIMEOUT);
                this.reader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()), BUFFER_SIZE);
                this.writer = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()), BUFFER_SIZE);
                synchronized (this.lock) {
                    this.lock.notifyAll();
                }
                this.callback.onConnected();
            } catch (final IOException ioe) {
                this.callback.onError(ioe);
                this.close();
            }
        });
    }

    public void asyncClose() {
        this.executor.execute(() -> {
            this.close();
        });
    }

    public void sendMessage(final String message) {
        this.executor.execute(() -> {
            if (this.writer != null) {
                try {
                    this.writer.write(message);
                    this.writer.write(LINE_TERMINATOR);
                    this.writer.flush();
                } catch (final IOException ioe) {
                    this.callback.onError(ioe);
                }
            }
        });
    }

    public ScheduledFuture<?> schedule(final Runnable runnable, final long delay) {
        return this.executor.scheduleAtFixedRate(runnable, 0, delay, TimeUnit.MILLISECONDS);
    }
}
