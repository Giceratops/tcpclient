package be.hogent.fnt.elm.auto.tcpclient;

/**
 * Created by giann on 2017-10-31.
 */

public interface TcpClientCallback {
    void onConnected();
    void onClosed();
    void onMessageReceived(final String message);
    void onError(final Exception e);
}
