package be.hogent.fnt.elm.auto.tcpclient;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;

public class MainActivity extends AppCompatActivity implements TcpClientCallback {

    private enum ConnectButtonState {
        CONNECT, DISCONNECT;
    }

    private final TcpClient client;

    private EditText txtHost, txtSend;
    private Button btnConnect, btnSend, btnInfo;
    private ScrollView svLog;
    private TextView txtLog;

    private ImageButton btnLed;
    private SeekBar sbRed, sbGreen, sbBlue;
    private Button btnADC;
    private ProgressBar[] prgADC = new ProgressBar[5];
    private ScheduledFuture<?> adcFuture;

    public MainActivity() {
        this.client = new TcpClient(this, Executors.newScheduledThreadPool(2));
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_main);

        this.txtHost = (EditText) super.findViewById(R.id.txtHost);
        this.txtSend = (EditText) super.findViewById(R.id.txtSend);

        this.svLog = (ScrollView) super.findViewById(R.id.svLog);
        this.txtLog = (TextView) super.findViewById(R.id.txtLog);

        this.btnInfo = (Button) super.findViewById(R.id.btnInfo);
        this.btnInfo.setOnClickListener(this::onInfo);

        this.btnConnect = (Button) super.findViewById(R.id.btnConnect);
        this.btnConnect.setOnClickListener(this::onConnect);

        this.btnSend = (Button) super.findViewById(R.id.btnSend);
        this.btnSend.setOnClickListener(this::onSend);

        this.btnLed = (ImageButton) super.findViewById(R.id.btnLed);
        this.btnLed.setOnClickListener(this::onLed);

        this.sbRed = (SeekBar) super.findViewById(R.id.sbRed);
        this.sbRed.setOnSeekBarChangeListener(new RGBSeekBarListener(this.client, RGBSeekBarListener.RGB.RED));
        this.sbGreen = (SeekBar) super.findViewById(R.id.sbGreen);
        this.sbGreen.setOnSeekBarChangeListener(new RGBSeekBarListener(this.client, RGBSeekBarListener.RGB.GREEN));
        this.sbBlue = (SeekBar) super.findViewById(R.id.sbBlue);
        this.sbBlue.setOnSeekBarChangeListener(new RGBSeekBarListener(this.client, RGBSeekBarListener.RGB.BLUE));

        this.btnADC = (Button) super.findViewById(R.id.btnADC);
        this.btnADC.setOnClickListener(this::onADC);

        this.prgADC[0] = (ProgressBar) super.findViewById(R.id.prgADC0);
        this.prgADC[1] = (ProgressBar) super.findViewById(R.id.prgADC1);
        this.prgADC[2] = (ProgressBar) super.findViewById(R.id.prgADC2);
        this.prgADC[3] = (ProgressBar) super.findViewById(R.id.prgADC3);
        this.prgADC[4] = (ProgressBar) super.findViewById(R.id.prgADC4);

        this.client.start();
        this.onClosed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        this.client.asyncClose();
    }

    @Override
    public void onConnected() {
        new Handler(Looper.getMainLooper()).post(() -> {
            this.btnConnect.setText("Disconnect");
            this.btnConnect.setTag(ConnectButtonState.DISCONNECT);
            this.btnConnect.setEnabled(true);

            this.txtHost.setEnabled(false);

            this.btnSend.setEnabled(true);
            this.txtSend.setEnabled(true);
            this.logToScreen("Connected");

            this.client.sendMessage("led status");
            this.client.sendMessage("rgb status");
            this.adcFuture = this.client.schedule(() -> {
                this.client.sendMessage("adc");
            }, 500);
        });
    }

    @Override
    public void onClosed() {
        new Handler(Looper.getMainLooper()).post(() -> {
            this.btnConnect.setText("Connect");
            this.btnConnect.setTag(ConnectButtonState.CONNECT);
            this.btnConnect.setEnabled(true);

            this.txtHost.setEnabled(true);

            this.btnSend.setEnabled(false);
            this.txtSend.setEnabled(false);

            if (this.adcFuture != null) {
                this.adcFuture.cancel(true);
            }

            this.logToScreen("Disconnected");
        });
    }

    @Override
    public void onMessageReceived(final String message) {
        new Handler(Looper.getMainLooper()).post(() -> {
            this.logToScreen(message);

            try (final Scanner sc = new Scanner(message)) {
                switch (sc.next()) {
                    case "led":
                        boolean on = sc.next().equals("on");
                        this.btnLed.setImageResource(on ? R.mipmap.onlamp : R.mipmap.offlamp);
                        this.btnLed.setTag(on);
                        break;
                    case "rgb":
                        while (sc.hasNext()) {
                            switch (sc.next()) {
                                case "r":
                                    this.sbRed.setProgress(sc.nextInt());
                                    break;
                                case "g":
                                    this.sbGreen.setProgress(sc.nextInt());
                                    break;
                                case "b":
                                    this.sbBlue.setProgress(sc.nextInt());
                                    break;
                            }
                        }
                        break;
                    case "adc":
                        for (byte b = 0; b < this.prgADC.length; b++) {
                            if (sc.hasNextInt()) {
                                this.prgADC[b].setVisibility(View.VISIBLE);
                                this.prgADC[b].setProgress(sc.nextInt());
                            } else {
                                this.prgADC[b].setVisibility(View.INVISIBLE);
                            }
                        }
                        break;
                }
            }
        });
    }

    @Override
    public void onError(final Exception e) {
        new Handler(Looper.getMainLooper()).post(() -> {
            this.logToScreen("Error: %s", e.toString());
        });
    }

    private void logToScreen(final String format, Object... args) {
        final String message = String.format(format, args);

        this.txtLog.append(message);
        if (!message.endsWith("\n")) {
            this.txtLog.append("\n");
        }

        this.svLog.post(() -> {
            this.svLog.fullScroll(View.FOCUS_DOWN);
        });
    }

    private void onInfo(final View v) {
        try {
            final List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (final NetworkInterface i : interfaces) {
                final List<InetAddress> addresses = Collections.list(i.getInetAddresses());
                if (!addresses.isEmpty()) {
                    this.logToScreen("Network interface found: %s", i.getDisplayName());

                    if (i.getHardwareAddress() != null) {
                        final StringBuilder sb = new StringBuilder();
                        for (final byte b : i.getHardwareAddress()) {
                            if (sb.length() > 0) {
                                sb.append(":");
                            }
                            sb.append(String.format("%02x", b));
                        }

                        this.logToScreen("\tMAC address: %s", sb.toString());
                    }
                    for (final InetAddress address : addresses) {
                        this.logToScreen("\tAddress: %s", address.getHostAddress());
                    }

                    this.logToScreen("");
                }
            }
        } catch (final Exception e) {
            e.printStackTrace(System.err);
        }
    }

    private void onConnect(final View v) {
        switch((ConnectButtonState) this.btnConnect.getTag()) {
            case CONNECT:
                try (final Scanner sc = new Scanner(this.txtHost.getText().toString())){
                    sc.useDelimiter(":");

                    this.logToScreen("Connecting....");
                    this.btnConnect.setEnabled(false);
                    this.txtHost.setEnabled(false);

                    this.client.asyncConnect(sc.next(), sc.nextInt());
                } catch (final Exception e) {
                    this.logToScreen("Error connecting: %s", e.toString());
                    e.printStackTrace(System.err);
                    this.onClosed();
                }
                break;
            case DISCONNECT:
                this.logToScreen("Disconnecting...");
                this.client.asyncClose();
                break;
        }
    }

    private void onSend(final View v) {
        this.client.sendMessage(this.txtSend.getText().toString());
        this.txtSend.setText("");
    }

    private void onLed(final View v) {
        if (this.btnLed.getTag() != null) {
            this.client.sendMessage("led " + (((boolean) this.btnLed.getTag()) ? "off" : "on"));
        }
    }

    private void onADC(final View v) {
        this.client.sendMessage("adc");
    }
}
