package be.hogent.fnt.elm.auto.tcpclient;

import android.widget.SeekBar;

/**
 * Created by giann on 2017-11-13.
 */

public class RGBSeekBarListener implements SeekBar.OnSeekBarChangeListener {

    enum RGB {RED, GREEN, BLUE};

    private final TcpClient client;
    private final RGB color;

    public RGBSeekBarListener(final TcpClient client, final RGB color) {
        this.client = client;
        this.color = color;
    }

    @Override
    public void onProgressChanged(final SeekBar seekBar, final int progress, final boolean fromUser) {
        if (fromUser) {
            this.client.sendMessage(String.format("rgb %s %d",
                    Character.toLowerCase(this.color.name().charAt(0)),
                    seekBar.getProgress()));
        }
    }

    @Override
    public void onStartTrackingTouch(final SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(final SeekBar seekBar) {
    }
};
